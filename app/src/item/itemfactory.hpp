#ifndef ITEMFACTORY_H
#define ITEMFACTORY_H

#include "component/componenttype.hpp"
#include "componentgraphicsitem.hpp"
#include "item/wiring/wireitem.hpp"
#include "scene.hpp"

class ItemFactory
{
public:
	static ItemFactory &Instance();

	[[nodiscard]] ComponentGraphicsItem *make(ComponentTypes type);
	[[nodiscard]] ComponentGraphicsItem *make(Component *component);

	template <typename First, typename Last>
	std::unordered_set<ComponentGraphicsItem *>
	deepCopyItemSelection(First itemOriginalsBegin, Last itemOriginalsEnd)
	{
		std::unordered_set<ComponentGraphicsItem *> itemCopies;

		std::vector<Component *> componentOriginals;

		std::transform(
			itemOriginalsBegin, itemOriginalsEnd, std::back_inserter(componentOriginals),
			[](ComponentGraphicsItem *originalItem) { return originalItem->m_component; });
		auto componentOriginalsBegin = std::begin(componentOriginals);
		auto componentOriginalsEnd = std::end(componentOriginals);
		CopyCache cache;
		for (auto itemToCopyIt = itemOriginalsBegin; itemToCopyIt != itemOriginalsEnd; itemToCopyIt++) {
			auto itemToCopy = *itemToCopyIt;
			auto componentToCopy = itemToCopy->m_component;
			auto componentCopied = ComponentFactory::instance().deepCopySelection(
				componentToCopy, cache, componentOriginalsBegin, componentOriginalsEnd, nullptr);
			ComponentGraphicsItem *itemCopied;
			if (componentCopied->getType() == ComponentTypes::WIRE) {
				auto *wi = static_cast<WireItem *>(itemToCopy);
				itemCopied = new WireItem(
					wi->pos().x() / Scene::UNIT, wi->pos().y() / Scene::UNIT, wi->m_width / Scene::UNIT,
					wi->m_height / Scene::UNIT, static_cast<Wire *>(componentCopied), wi->getDirection());
			} else {
				itemCopied = ItemFactory::Instance().make(componentCopied);
			}
			itemCopied->changeOrientation(itemToCopy->m_orientation);
			itemCopied->setPos(itemToCopy->pos());
			itemCopies.insert(itemCopied);
		}

		return itemCopies;
	}

private:
	ItemFactory();
	ItemFactory(const ItemFactory &other) = delete;
};

#endif // ITEMFACTORY_H
