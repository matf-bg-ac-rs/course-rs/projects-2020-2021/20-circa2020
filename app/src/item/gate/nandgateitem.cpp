#include "item/gate/nandgateitem.hpp"

NandGateItem::NandGateItem(int posX, int posY, QGraphicsItem *parent)
	: NandGateItem(ComponentFactory::instance().makeNandGate(), posX, posY, parent)
{
}

NandGateItem::NandGateItem(Component *component, int posX, int posY, QGraphicsItem *parent)
	: GateItem(component, posX, posY, 0, 0, parent)
{
	if ((component == nullptr) || component->getType() != ComponentTypes::NANDGATE)
		throw "Incompatible component for this item";
	m_isResizable = true;
	this->resizeInputs(m_component->getInputSize());
}

int NandGateItem::calculateHeight() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT)
		return m_component->getInputSize() * 2;
	else
		return int(calculateWidth() * Scene::UNIT * 0.75) / Scene::UNIT + 2;
}

int NandGateItem::calculateWidth() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT)
		return int(m_height * 0.75) / Scene::UNIT + 2;
	else
		return m_component->getInputSize() * 2;
}

void NandGateItem::calculatePath()
{
	if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT) {
		m_path = QPainterPath(QPoint(0, 0));
		m_path.lineTo(0, m_height);
		m_path.lineTo(m_width / 2, m_height);
		m_path.arcTo(0, 0, m_width - Scene::UNIT - 5, m_height, -90, 180);
		m_path.lineTo(0, 0);
		m_path.addEllipse(m_width - Scene::UNIT - 5, m_height / 2 - Scene::UNIT / 2, Scene::UNIT, Scene::UNIT);
	} else {
		m_path = QPainterPath(QPoint(m_width, m_height));
		m_path.lineTo(0, m_height);
		m_path.lineTo(0, 3 * m_height / 4);
		m_path.arcTo(0, 3 * Scene::UNIT / 2, m_width, m_height - Scene::UNIT - 5, -180, -180);
		m_path.lineTo(m_width, m_height);
		m_path.addEllipse(m_width / 2 - Scene::UNIT / 2, Scene::UNIT / 2, Scene::UNIT, Scene::UNIT);
	}

	if (m_orientation == Direction::LEFT || m_orientation == Direction::DOWN) {
		QTransform t;
		t.rotate(180);
		t.translate(-m_width, -m_height);
		m_path = t.map(m_path);
	}
}
