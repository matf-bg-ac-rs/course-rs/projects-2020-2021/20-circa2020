#ifndef NOTGATEITEM_H
#define NOTGATEITEM_H

#include "gateitem.hpp"

class NotGateItem : public GateItem
{
public:
	NotGateItem(int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);
	NotGateItem(Component *component, int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);

protected:
	int calculateWidth() const override;
	int calculateHeight() const override;
	void calculatePath() override;
};

#endif // NOTGATEITEM_H
