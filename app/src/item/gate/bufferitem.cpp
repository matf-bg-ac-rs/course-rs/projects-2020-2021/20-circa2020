#include "item/gate/bufferitem.hpp"

BufferItem::BufferItem(int posX, int posY, QGraphicsItem *parent)
	: BufferItem(ComponentFactory::instance().makeBuffer(), posX, posY, parent)
{
}

BufferItem::BufferItem(Component *component, int posX, int posY, QGraphicsItem *parent)
	: GateItem(component, posX, posY, 4, 2, parent)
{
	if ((component == nullptr) || component->getType() != ComponentTypes::BUFFER)
		throw "Incompatible component for this item";
	m_isResizable = false;
	this->resizeInputs(m_component->getInputSize());
}

int BufferItem::calculateHeight() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT)
		return m_component->getInputSize() * 2;
	else
		return int(calculateWidth() * Scene::UNIT * 1.5) / Scene::UNIT + 1;
}

int BufferItem::calculateWidth() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT)
		return int(m_height * 1.5) / Scene::UNIT + 1;
	else
		return m_component->getInputSize() * 2;
}

void BufferItem::calculatePath()
{
	if (m_orientation == Direction::RIGHT) {
		m_path = QPainterPath(QPoint(0, 0));
		m_path.lineTo(m_width, m_height / 2);
		m_path.lineTo(0, m_height);
		m_path.lineTo(0, 0);
	} else if (m_orientation == Direction::DOWN) {
		m_path = QPainterPath(QPoint(0, 0));
		m_path.lineTo(m_width, 0);
		m_path.lineTo(m_width / 2, m_height);
		m_path.lineTo(0, 0);
	} else if (m_orientation == Direction::LEFT) {
		m_path = QPainterPath(QPoint(m_width, 0));
		m_path.lineTo(m_width, m_height);
		m_path.lineTo(0, m_height / 2);
		m_path.lineTo(m_width, 0);
	} else {
		m_path = QPainterPath(QPoint(m_width / 2, 0));
		m_path.lineTo(0, m_height);
		m_path.lineTo(m_width, m_height);
		m_path.lineTo(m_width / 2, 0);
	}
}
