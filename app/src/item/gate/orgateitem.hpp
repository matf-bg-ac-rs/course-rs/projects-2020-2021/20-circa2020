#ifndef ORGATEITEM_H
#define ORGATEITEM_H

#include "gateitem.hpp"

class OrGateItem : public GateItem
{
public:
	OrGateItem(int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);
	OrGateItem(Component *component, int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);

protected:
	int calculateWidth() const override;
	int calculateHeight() const override;
	void calculatePath() override;
};

#endif // ORGATEITEM_H
