#ifndef CONSTANTITEM_H
#define CONSTANTITEM_H

#include "item/componentgraphicsitem.hpp"

class ConstantItem : public ComponentGraphicsItem
{
public:
	ConstantItem(int posX = 0, int posY = 0, int width = 2, QGraphicsItem *parent = nullptr);
	ConstantItem(int posX, int posY, int width, Component *component, QGraphicsItem *parent = nullptr);

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

	void drawOrientation() override;
};

#endif // CONSTANTITEM_H
