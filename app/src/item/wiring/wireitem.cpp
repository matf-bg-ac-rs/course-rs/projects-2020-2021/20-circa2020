#include "item/wiring/wireitem.hpp"

#include "util/colorutil.hpp"

WireItem::WireItem(QGraphicsItem *parent)
	: WireItem(0, 0, 0, 0, ComponentFactory::instance().makeWire(), Direction::RIGHT, parent)
{
	throw "Not implemented";
}

WireItem::WireItem(
	int posX, int posY, int width, int height, Wire *component, const Direction &direction, QGraphicsItem *parent)
	: ComponentGraphicsItem(posX, posY, width, height, component, parent)
{
	if ((component == nullptr) || component->getType() != ComponentTypes::WIRE)
		throw "Incompatible component for this item";
	this->m_isResizable = false;
	this->m_direction = direction;
	this->m_wire = component;
	refreshConnectionPoints();
}

void WireItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(option);
	Q_UNUSED(widget);
	auto col = ColorUtil::forState(m_wire->getState());

	QPen p;
	p.setColor(col);
	p.setWidth(3);
	painter->setPen(p);
	painter->drawLine(0, 0, m_width, m_height);

	ComponentGraphicsItem::paint(painter, option, widget);
}

Direction WireItem::getDirection()
{
	return m_direction;
}

void WireItem::refreshConnectionPoints()
{
	Direction dir1 = DirectionUtil::horizontal(m_direction) ? Direction::LEFT : Direction::UP;
	Direction dir2 = DirectionUtil::horizontal(m_direction) ? Direction::RIGHT : Direction::DOWN;
	m_inputPoints = {ConnectionPoint(0, 0, Scene::POINT_SIZE, dir1)};
	m_outputPoints = {ConnectionPoint(m_width, m_height, Scene::POINT_SIZE, dir2)};
}

QRectF WireItem::boundingRect() const
{
	return QRectF(
		-Scene::POINT_SIZE / 2, -Scene::POINT_SIZE / 2, m_width + Scene::POINT_SIZE, m_height + Scene::POINT_SIZE);
}

QRectF WireItem::overlapBoundingRect() const
{
	return boundingRect();
}

QRectF WireItem::overlapBoundingRectSc() const
{
	return overlapBoundingRect().translated(scenePos());
}

void WireItem::serialize(std::ostream &os) const
{
	ComponentGraphicsItem::serialize(os);
	DirectionUtil::serialize(os, m_direction);
}

void WireItem::deserialize(std::istream &is)
{
	ComponentGraphicsItem::deserialize(is);
	m_direction = DirectionUtil::deserialize(is);
	m_wire = static_cast<Wire *>(m_component);

	refreshConnectionPoints();
}

/**
 * @brief WireItem::splitMe
 * @return
 */
std::tuple<WireItem *, Direction, Direction> WireItem::splitMe(const QPointF &break_point)
{
	auto tag = "[" + m_component->short_identify() + "][SPLIT]";
	auto *scene = getScene();
	auto *new_wire = ComponentFactory::instance().makeWire();
	WireItem *wi;
	std::tuple<WireItem *, Direction, Direction> tup;

	prepareGeometryChange();

	if (m_direction == Direction::LEFT || m_direction == Direction::RIGHT) {
		std::cerr << tag << " Performing horizontal wire split." << std::endl;
		for (auto i = 3; i <= 5; i++) {
			if (m_wire->getInputConnections()[i].connected) {
				new_wire->getInputConnections()[i] = m_wire->getInputConnections()[i];
				new_wire->getInputConnections()[i]
					.to_component->getOutputConnections()[m_wire->getInputConnections()[i].to_index]
					.to_component = new_wire;
				new_wire->getInputConnections()[i]
					.to_component->getOutputConnections()[m_wire->getInputConnections()[i].to_index]
					.to_index = i;
				m_wire->getInputConnections()[i].setSignal(Signal::State::UNDEFINED);
			}
			if (m_wire->getOutputConnections()[i].connected) {
				new_wire->getOutputConnections()[i] = m_wire->getOutputConnections()[i];
				new_wire->getOutputConnections()[i]
					.to_component->getInputConnections()[m_wire->getOutputConnections()[i].to_index]
					.to_component = new_wire;
				new_wire->getOutputConnections()[i]
					.to_component->getInputConnections()[m_wire->getOutputConnections()[i].to_index]
					.to_index = i;
				m_wire->getInputConnections()[i].setSignal(Signal::State::UNDEFINED);
			}
			m_wire->getInputConnections()[i].connected = false;
			m_wire->getOutputConnections()[i].connected = false;
		}

		ConnectionManager::Instance().connect(m_wire, 3, new_wire, 0);
		ConnectionManager::Instance().connect(new_wire, 0, m_wire, 3);

		auto total_width = m_width;
		m_width = (break_point.x() - scenePos().x());

		wi = new WireItem(
			(pos().x() + m_width) / Scene::UNIT, pos().y() / Scene::UNIT, (total_width - m_width) / Scene::UNIT,
			m_height / Scene::UNIT, new_wire, m_direction);
		scene->addComponent(wi);

		refreshConnectionPoints();
		tup = {wi, Direction::RIGHT, Direction::LEFT};

	} else if (m_direction == Direction::UP || m_direction == Direction::DOWN) {
		std::cerr << tag << " Performing vertical wire split." << std::endl;
		for (auto i = 0; i <= 2; i++) {
			if (m_wire->getInputConnections()[i].connected) {
				new_wire->getInputConnections()[i] = m_wire->getInputConnections()[i];
				new_wire->getInputConnections()[i]
					.to_component->getOutputConnections()[m_wire->getInputConnections()[i].to_index]
					.to_component = new_wire;
				new_wire->getInputConnections()[i]
					.to_component->getOutputConnections()[m_wire->getInputConnections()[i].to_index]
					.to_index = i;
				m_wire->getInputConnections()[i].setSignal(Signal::State::UNDEFINED);
			}
			if (m_wire->getOutputConnections()[i].connected) {
				new_wire->getOutputConnections()[i] = m_wire->getOutputConnections()[i];
				new_wire->getOutputConnections()[i]
					.to_component->getInputConnections()[m_wire->getOutputConnections()[i].to_index]
					.to_component = new_wire;
				new_wire->getOutputConnections()[i]
					.to_component->getInputConnections()[m_wire->getOutputConnections()[i].to_index]
					.to_index = i;
				m_wire->getInputConnections()[i].setSignal(Signal::State::UNDEFINED);
			}
			m_wire->getInputConnections()[i].connected = false;
			m_wire->getOutputConnections()[i].connected = false;
		}

		ConnectionManager::Instance().connect(m_wire, 0, new_wire, 3);
		ConnectionManager::Instance().connect(new_wire, 3, m_wire, 0);

		auto total_height = m_height;
		m_height = (break_point.y() - scenePos().y());

		wi = new WireItem(
			pos().x() / Scene::UNIT, (pos().y() + m_height) / Scene::UNIT, m_width / Scene::UNIT,
			(total_height - m_height) / Scene::UNIT, new_wire, m_direction);
		scene->addComponent(wi);

		refreshConnectionPoints();
		tup = {wi, Direction::DOWN, Direction::UP};
	}
	return tup;
}
