#include "item/wiring/lightbulbitem.hpp"

#include "util/colorutil.hpp"

LightBulbItem::LightBulbItem(int posX, int posY, int width, QGraphicsItem *parent)
	: LightBulbItem(posX, posY, width, ComponentFactory::instance().makeLightBulb(), parent)
{
}

LightBulbItem::LightBulbItem(int posX, int posY, int width, Component *component, QGraphicsItem *parent)
	: ComponentGraphicsItem(posX, posY, width, width, component, parent)
{
	if (component->getType() != ComponentTypes::LIGHTBULB)
		throw "Incompatible component for this item";
	m_isResizable = false;

	drawOrientation();
}

void LightBulbItem::drawOrientation()
{
	if (m_orientation == Direction::RIGHT) {
		m_inputPoints = {ConnectionPoint(0, m_height / 2, Direction::LEFT)};
	} else if (m_orientation == Direction::DOWN) {
		m_inputPoints = {ConnectionPoint(m_width / 2, 0, Direction::UP)};
	} else if (m_orientation == Direction::LEFT) {
		m_inputPoints = {ConnectionPoint(m_width, m_height / 2, Direction::RIGHT)};
	} else {
		m_inputPoints = {ConnectionPoint(m_width / 2, m_height, Direction::DOWN)};
	}
}

void LightBulbItem::resizeInputs(unsigned /*n*/)
{
	throw "LightBulb must have one input!";
}

void LightBulbItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	//    painter->setPen(Qt::yellow);
	//    painter->drawRect(boundingRect());

	QPen p = QPen();
	p.setWidth(2);
	painter->setPen(p);
	QBrush b = QBrush(ColorUtil::forState(m_component->getInput(0).getState()));
	painter->setBrush(b);

	painter->drawEllipse(0, 0, m_width, m_height);

	ComponentGraphicsItem::paint(painter, option, widget);
}
