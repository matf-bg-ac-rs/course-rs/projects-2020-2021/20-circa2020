#include "clockitem.hpp"

#include "util/colorutil.hpp"

ClockItem::ClockItem(int posX, int posY, int width, QGraphicsItem *parent)
	: ClockItem(posX, posY, width, ComponentFactory::instance().makeClock(), parent)
{
}

ClockItem::ClockItem(int posX, int posY, int width, Component *component, QGraphicsItem *parent)
	: ComponentGraphicsItem(posX, posY, width, width, component, parent)
{
	if (component->getType() != ComponentTypes::CLOCK)
		throw "Incompatible component for this item";
	m_isResizable = false;
	m_outputPoints = {ConnectionPoint(m_width, m_height / 2, Direction::RIGHT)};
	flipSwitch = QRectF(m_width / 4, m_height / 4, m_width / 2, m_height / 2);

	pathOff.moveTo(m_width / 4, 3 * m_height / 4);
	pathOff.lineTo(m_width / 2, 3 * m_height / 4);
	pathOff.lineTo(m_width / 2, m_height / 4);
	pathOff.lineTo(3 * m_width / 4, m_height / 4);

	pathOn.moveTo(m_width / 4, m_height / 4);
	pathOn.lineTo(m_width / 2, m_height / 4);
	pathOn.lineTo(m_width / 2, 3 * m_height / 4);
	pathOn.lineTo(3 * m_width / 4, 3 * m_height / 4);
}

void ClockItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	//    painter->setPen(Qt::yellow);
	//    painter->drawRect(boundingRect());

	QPen p = QPen();
	p.setWidth(2);
	painter->setPen(p);
	painter->drawRect(0, 0, m_width, m_height);

	p.setWidth(3);
	p.setColor(ColorUtil::forState(m_component->getOutput(0).getState()));
	painter->setPen(p);

	if (m_component->getOutput(0).getState() == Signal::State::FALSE)
		painter->drawPath(pathOff);
	else if (m_component->getOutput(0).getState() == Signal::State::TRUE)
		painter->drawPath(pathOn);

	ComponentGraphicsItem::paint(painter, option, widget);
}

void ClockItem::drawOrientation()
{
	if (m_orientation == Direction::RIGHT) {
		m_outputPoints = {ConnectionPoint(m_width, m_height / 2, Direction::RIGHT)};
		flipSwitch = QRectF(m_width / 4, m_height / 4, m_width / 2, m_height / 2);
	} else if (m_orientation == Direction::LEFT) {
		m_outputPoints = {ConnectionPoint(0, m_height / 2, Direction::LEFT)};
		flipSwitch = QRectF(m_width / 4, m_height / 4, m_width / 2, m_height / 2);
	} else if (m_orientation == Direction::DOWN) {
		m_outputPoints = {ConnectionPoint(m_width / 2, m_height, Direction::DOWN)};
		flipSwitch = QRectF(m_width / 4, m_height / 4, m_width / 2, m_height / 2);
	} else {
		m_outputPoints = {ConnectionPoint(m_width / 2, 0, Direction::UP)};
		flipSwitch = QRectF(m_width / 4, m_height / 4, m_width / 2, m_height / 2);
	}
}

void ClockItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	if (getScene()->controlMode() == ControlMode::PINS && flipSwitch.contains(event->pos())) {
		static_cast<Clock *>(m_component)->tick();
		update(boundingRect());
	} else {
		ComponentGraphicsItem::mousePressEvent(event);
	}
}
