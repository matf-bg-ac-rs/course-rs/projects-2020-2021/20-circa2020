#include "gate/andgate.hpp"

#include "catch.hpp"

TEST_CASE("And Gate Constructor", "[andgate][init][unit]")
{

	SECTION("Default", "Make a 2 input gate")
	{
		AndGate gate;
		REQUIRE(gate.getInputSize() == 2);
		REQUIRE(gate.getOutputSize() == 1);
	}

	SECTION("Given input size", "Make a 5 input gate")
	{
		AndGate gate = AndGate(5);
		REQUIRE(gate.getInputSize() == 5);
		REQUIRE(gate.getOutputSize() == 1);
	}
}

TEST_CASE("And Gate Update", "[andgate][update][unit]")
{
	AndGate gate;

	REQUIRE(gate.getOutput(0).getState() == Signal::State::UNDEFINED);

	SECTION("Input error", "One input is error")
	{
		gate.setInput(1, Signal::State::ERROR);

		gate.setInput(0, Signal::State::FALSE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::ERROR);

		gate.setInput(0, Signal::State::TRUE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::ERROR);

		gate.setInput(0, Signal::State::UNDEFINED);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::ERROR);
	}

	SECTION("Input undefined", "One input is undefined")
	{
		gate.setInput(1, Signal::State::UNDEFINED);

		gate.setInput(0, Signal::State::FALSE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::FALSE);

		gate.setInput(0, Signal::State::TRUE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::UNDEFINED);

		gate.setInput(0, Signal::State::UNDEFINED);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::UNDEFINED);
	}

	SECTION("Positive output", "Output should be true")
	{
		gate.setInput(0, Signal::State::TRUE);
		gate.setInput(1, Signal::State::TRUE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::TRUE);
	}

	SECTION("Negative output", "Output should be false")
	{
		gate.setInput(0, Signal::State::FALSE);
		gate.setInput(1, Signal::State::FALSE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::FALSE);

		gate.setInput(0, Signal::State::TRUE);
		gate.setInput(1, Signal::State::FALSE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::FALSE);

		gate.setInput(0, Signal::State::FALSE);
		gate.setInput(1, Signal::State::TRUE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::FALSE);
	}
}

TEST_CASE("And Gate Resize Inputs", "[andgate][resize][unit]")
{
	AndGate gate;
	gate.setInput(0, Signal::State::TRUE);
	gate.setInput(1, Signal::State::TRUE);

	gate.resizeInputs(4);
	REQUIRE(gate.getInputSize() == 4);
	REQUIRE(gate.getOutput(0).getState() == Signal::State::UNDEFINED);

	gate.resizeInputs(2);
	REQUIRE(gate.getInputSize() == 2);
	REQUIRE(gate.getOutput(0).getState() == Signal::State::TRUE);

	REQUIRE_THROWS(gate.resizeInputs(1));
}

TEST_CASE("And Gate Resize Outputs", "[andgate][resize][unit]")
{
	AndGate gate;
	REQUIRE_THROWS(gate.resizeOutputs(2));
}
