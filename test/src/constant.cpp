#include "wiring/constant.hpp"

#include "catch.hpp"

TEST_CASE("Constant Constructor", "[constant][init][unit]")
{
	Constant gate1 = Constant(Signal::State::TRUE);
	REQUIRE(gate1.getInputSize() == 0);
	REQUIRE(gate1.getOutputSize() == 1);
	REQUIRE(gate1.getOutput(0).getState() == Signal::State::TRUE);

	Constant gate2 = Constant(Signal::State::FALSE);
	REQUIRE(gate2.getInputSize() == 0);
	REQUIRE(gate2.getOutputSize() == 1);
	REQUIRE(gate2.getOutput(0).getState() == Signal::State::FALSE);

	Constant gate3 = Constant(Signal::State::UNDEFINED);
	REQUIRE(gate3.getInputSize() == 0);
	REQUIRE(gate3.getOutputSize() == 1);
	REQUIRE(gate3.getOutput(0).getState() == Signal::State::UNDEFINED);

	Constant gate4 = Constant(Signal::State::ERROR);
	REQUIRE(gate4.getInputSize() == 0);
	REQUIRE(gate4.getOutputSize() == 1);
	REQUIRE(gate4.getOutput(0).getState() == Signal::State::ERROR);
}

TEST_CASE("Constant Resize Inputs", "[constant][resize][unit]")
{
	Constant gate = Constant(Signal::State::TRUE);
	REQUIRE_THROWS(gate.resizeInputs(1));
}

TEST_CASE("Constant Resize Outputs", "[constant][resize][unit]")
{
	Constant gate = Constant(Signal::State::TRUE);
	REQUIRE_THROWS(gate.resizeOutputs(2));
}
