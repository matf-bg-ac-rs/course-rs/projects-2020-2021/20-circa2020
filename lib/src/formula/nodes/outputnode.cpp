#include "outputnode.hpp"

OutputNode::OutputNode(int id, OpNode *input) : m_id(id), m_input(input)
{
}

OutputNode::~OutputNode()
{
	delete m_input;
}

std::string OutputNode::toString() const
{
	return "out[" + std::to_string(m_id) + "] = " + m_input->toString();
}

OpNode *OutputNode::simplify()
{
	return new OutputNode(m_id, m_input->simplify());
}

OpNode *OutputNode::getInput() const
{
	return m_input;
}

OpType OutputNode::getType() const
{
	return OpType::OUTPUT;
}

bool OutputNode::isEqual(OpNode *other) const
{
	auto *node = static_cast<OutputNode *>(other);
	return equals(m_input, node->getInput());
}
