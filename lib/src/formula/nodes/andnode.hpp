#ifndef ANDNODE_H
#define ANDNODE_H

#include <string>
#include <vector>

#include "opnode.hpp"

class AndNode : public OpNode
{
public:
	AndNode(std::vector<OpNode *> m_inputs);
	~AndNode() override;

	std::string toString() const override;
	OpNode *simplify() override;

	std::vector<OpNode *> getInputs() const;

	OpType getType() const override;

protected:
	bool isEqual(OpNode *other) const override;

private:
	std::vector<OpNode *> m_inputs;
};

#endif // ANDNODE_H
