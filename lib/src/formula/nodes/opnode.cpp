#include "opnode.hpp"

OpNode::OpNode() = default;

bool OpNode::equals(OpNode *lhs, OpNode *rhs)
{
	return lhs->getType() == rhs->getType() && lhs->isEqual(rhs);
}

int OpNode::getPriority(OpType type)
{
	switch (type) {
	case OpType::INPUT:
	case OpType::CONSTANT:
	case OpType::BUFFER:
		return 1;
	case OpType::NOT:
		return 2;
	case OpType::AND:
		return 3;
	case OpType::XOR:
		return 4;
	case OpType::OR:
		return 5;
	case OpType::OUTPUT:
		return 6;
	}
	throw "Unknown type";
}

OpNode::~OpNode() = default;
