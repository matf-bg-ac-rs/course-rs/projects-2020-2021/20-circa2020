#ifndef XORNODE_H
#define XORNODE_H

#include <string>
#include <vector>

#include "opnode.hpp"

class XorNode : public OpNode
{
public:
	XorNode(std::vector<OpNode *> inputs);
	~XorNode() override;

	std::string toString() const override;
	OpNode *simplify() override;

	std::vector<OpNode *> getInputs() const;

	OpType getType() const override;

protected:
	bool isEqual(OpNode *other) const override;

private:
	std::vector<OpNode *> m_inputs;
};

#endif // XORNODE_H
