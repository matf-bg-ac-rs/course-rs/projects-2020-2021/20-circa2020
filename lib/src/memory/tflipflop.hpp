#ifndef TFLIPFLOP_H
#define TFLIPFLOP_H

#include "component/component.hpp"

class TFlipFlop : public Component
{
public:
	TFlipFlop();
	~TFlipFlop() override;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	void update() override;
};

#endif // TFLIPFLOP_H
