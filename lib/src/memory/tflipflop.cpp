#include "tflipflop.hpp"

TFlipFlop::TFlipFlop() : Component(2, 2)
{

	output_connections[0].setSignal(Signal::State::FALSE);
	output_connections[1].setSignal(Signal::State::TRUE);
}

TFlipFlop::~TFlipFlop() = default;

void TFlipFlop::resizeInputs(unsigned n)
{
	if (n != 2)
		throw "Resize inputs for T flip-flop is not allowed!";
}

void TFlipFlop::resizeOutputs(unsigned n)
{
	if (n != 2)
		throw "Resize outputs for T flip-flop is not allowed!";
}

void TFlipFlop::update()
{

	for (auto &conn : input_connections) {
		if (conn.getSignal().getState() == Signal::State::ERROR) {
			output_connections[0].setSignal(Signal::State::ERROR);
			output_connections[1].setSignal(Signal::State::ERROR);
			return;
		}
	}

	if (input_connections[0].getSignal().getState() == Signal::State::FALSE)
		return;

	if (input_connections[1].getSignal().getState() == Signal::State::FALSE)
		return;

	if (input_connections[0].getSignal().getState() == Signal::State::TRUE &&
		input_connections[1].getSignal().getState() == Signal::State::TRUE) {
		for (auto &conn : output_connections)
			conn.setSignal(!conn.getSignal());
	}
}

ComponentTypes TFlipFlop::getType() const
{
	return ComponentTypes::TFLIPFLOP;
}
