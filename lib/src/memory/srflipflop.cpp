#include "srflipflop.hpp"

SRFlipFlop::SRFlipFlop() : Component(2, 2)
{
}

SRFlipFlop::~SRFlipFlop() = default;

void SRFlipFlop::resizeInputs(unsigned n)
{
	if (n != 2)
		throw "Resize inputs for SR flip-flop is not allowed!";
}

void SRFlipFlop::resizeOutputs(unsigned n)
{
	if (n != 2)
		throw "Resize outputs for SR flip-flop is not allowed!";
}

void SRFlipFlop::update()
{

	for (auto &conn : input_connections) {
		if (conn.getSignal().getState() == Signal::State::ERROR) {
			output_connections[0].setSignal(Signal::State::ERROR);
			output_connections[1].setSignal(Signal::State::ERROR);
			return;
		}
	}

	if (this->input_connections[0].getSignal().getState() == Signal::State::FALSE &&
		this->input_connections[1].getSignal().getState() == Signal::State::FALSE)
		return;

	if (this->input_connections[0].getSignal().getState() == Signal::State::FALSE &&
		this->input_connections[1].getSignal().getState() == Signal::State::TRUE) {
		this->output_connections[0].setSignal(Signal::State::FALSE);
		this->output_connections[1].setSignal(Signal::State::TRUE);
	}

	if (this->input_connections[0].getSignal().getState() == Signal::State::TRUE &&
		this->input_connections[1].getSignal().getState() == Signal::State::FALSE) {
		this->output_connections[0].setSignal(Signal::State::TRUE);
		this->output_connections[1].setSignal(Signal::State::FALSE);
	}

	if (this->input_connections[0].getSignal().getState() == Signal::State::TRUE &&
		this->input_connections[1].getSignal().getState() == Signal::State::TRUE) {
		this->output_connections[0].setSignal(Signal::State::UNDEFINED);
		this->output_connections[1].setSignal(Signal::State::UNDEFINED);
	}
}

ComponentTypes SRFlipFlop::getType() const
{
	return ComponentTypes::SRFLIPFLOP;
}
