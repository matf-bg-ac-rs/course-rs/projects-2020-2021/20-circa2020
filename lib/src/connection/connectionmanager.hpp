#ifndef CONNECTIONMANAGER_H
#define CONNECTIONMANAGER_H

#include "component/component.hpp"

class ConnectionManager
{
public:
	static ConnectionManager &Instance();

	bool connect(Component *output, unsigned output_index, Component *input, unsigned input_index);
	bool connect(Component *output, unsigned output_index, Component *input, unsigned input_index, bool direction);

	bool disconnectInput(Component *component, unsigned index);
	void disconnectInputs(Component *component, unsigned start, unsigned end);
	void disconnectInputs(Component *component);

	bool disconnectOutput(Component *component, unsigned index);
	void disconnectOutputs(Component *component, unsigned start, unsigned end);
	void disconnectOutputs(Component *component);

	void moveInput(Component *component, unsigned index_from, unsigned index_to, bool update = true);
	void transferWireConnection(Component *from, Component *to, unsigned index);

private:
	ConnectionManager() = default;
	;
	ConnectionManager(const ConnectionManager &other) = delete;

	void transferInput(Component *from, unsigned from_index, Component *to, unsigned to_index, bool update = true);
	void
	transferOutput(Component *from, unsigned from_index, Component *to, unsigned to_index, bool update = true);
};

#endif // CONNECTIONMANAGER_H
