#ifndef ANDGATE_H
#define ANDGATE_H

#include "gate.hpp"

class AndGate : public Gate
{
public:
	AndGate(unsigned nInputs = 2);
	~AndGate() override;

	ComponentTypes getType() const override;

protected:
	Signal op() override;
};

#endif // ANDGATE_H
