#include "buffer.hpp"

Buffer::Buffer() : Gate(1)
{
}

Buffer::~Buffer() = default;

Signal Buffer::op()
{
	return this->input_connections[0].getSignal();
}

ComponentTypes Buffer::getType() const
{
	return ComponentTypes::BUFFER;
}

void Buffer::resizeInputs(unsigned n)
{
	if (n != 1)
		throw "Buffer must have just one input!";
}

void Buffer::resizeOutputs(unsigned n)
{
	if (n != 1)
		throw "Buffer mush have just one output!";
}
