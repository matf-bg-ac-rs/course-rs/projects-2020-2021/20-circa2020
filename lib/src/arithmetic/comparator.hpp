#ifndef COMPARATOR_H
#define COMPARATOR_H

#include "component/component.hpp"

class Comparator : public Component
{
public:
	Comparator();
	~Comparator() override = default;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	void update() override;
};

#endif // COMPARATOR_H
