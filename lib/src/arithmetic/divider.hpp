#ifndef DIVIDER_H
#define DIVIDER_H

#include "component/component.hpp"

class Divider : public Component
{
public:
	Divider();
	~Divider() override = default;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	void update() override;
};

#endif // DIVIDER_H
