#include "lightbulb.hpp"

LightBulb::LightBulb() : Component(1, 0)
{
}

LightBulb::~LightBulb() = default;

Signal LightBulb::getOutput()
{
	return this->input_connections[0].getSignal();
}

void LightBulb::update()
{
}

ComponentTypes LightBulb::getType() const
{
	return ComponentTypes::LIGHTBULB;
}

void LightBulb::resizeInputs(unsigned /*unused*/)
{
	throw "Lightbulb mush have just one input!";
}

void LightBulb::resizeOutputs(unsigned /*unused*/)
{
	throw "Lighbulb has no outputs!";
}
